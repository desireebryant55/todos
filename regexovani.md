# Regexování

Regulární výrazy vycházejí z celé jedné vědní disciplíny, někde nazývané Automaty a Gramatiky. Regulární výraz je stringem representovaný Automat. Radši dost, nebo začnu plácat nesmysly.

**motivace**

* Vyseparování dat v idečku z na první pohled komplexního textu (html, csv, json, log, ...)
* Libovolně rozsáhlé logy s důležitými daty
* Nahrazení textu textem v nejoptimálnějším čase
* Rychlejší než sken textu očima 
* ...

## Základy - 15 minut

Regulární výrazy používáme především jako test, jestli vstup splňuje pattern, toto nazýváme matchnutím/testem. Druhý případ užití regexu je ve vytažení podstringu ze vstupu, k tomu slouží regex groups. 

Regexy je možné psát téměř v libovolném jazyce:
* Java
```java
"tadeas.musil@synetech.cz".matches(".*\..*@synetech\.cz")
```

* Javascript
```javascript
const isMatch = /.*\..*@synetech\.cz/.exec("tadeas.musil@synetech.cz");
```

* Kotlin
```kotlin
val text = "I saw a fox in the wood. The fox had red fur."
val pattern = "fox".toRegex()
val match = pattern.find(text)
```

* Swift
```swift
var invitation = "Hey, would you like to play Clou?"
invitation.contains(".ou")
```

* Bash
```bash
$ echo "ahoj svete, tohle je hodne ee" | grep "e"
```

### Základní znaky

* jeden libovolný znak `.`
```javascript
const isMatch = /tadeas\.musil.synetech\.cz/.exec("tadeas.musil@synetech.cz");
```
* jeden z množiny konkrétních znaků `[jedn]`, `[2-7]`, `[d-x]`, `[d-xD-X]`
```javascript
const isMatch = /[a-zA-Z]adeas\.musil@synetech\.cz/.exec("tadeas.musil@synetech.cz");
```
* jeden libovolný kromě `[^x]`
```javascript
const isMatch = /[^.][a-zA-Z]adeas\.musil@synetech\.cz/.exec("tadeas.musil@synetech.cz");
```
* libovolný počet opakování předchozího `*`
```javascript
const isMatch = /[^.][a-zA-Z]*\.musil@synetech\.cz/.exec("tadeas.musil@synetech.cz");
```
* kokrétní počet opakování přechozího `{2}`, `{2,}`, `{,2}` 
```javascript
const isMatch = /[^.][a-zA-Z]{5,}\.musil@synetech\.cz/.exec("tadeas.musil@synetech.cz");
```
* začátek řádku `^`
```javascript
const isMatch = /^tadeas.*/.exec("tadeas.musil@synetech.cz");
```
* konec řádku `$`
```javascript
const isMatch = /.*\.cz$/.exec("tadeas.musil@synetech.cz");
```

### Regex options

Jedná se o jisté nastavení, jak se regex bude chovat při matchování patternu. Zpravidla jej píšeme na konec. Můžeme s ním ovlivnit nepřebernou škálu vlastností, například jestli se bude respektovat rozdíl mezi velkými/malými znaky, zda se pattern aplikuje v multiline/single line režimu, atd.. Vždy se těmto nastavením dá vyhnout správným zapsáním patternu, což doporučuji alespoň ze začátku.

## Zástupné znaky včetně výčtu populárních - 10 min.

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Character_Classes

* celé číslo `\d`
* aplhaumerický znak `\w`
* bílý znak `\s`
* NEbílý znak `\S`
* ...

Zpravidla pokud za lomítko napíšete Uppercase znak, myslíte tím právě opak

## Testing vs grouping - 20 min.

Část patternu můžeme označit v kulatých závorkách. Skupin v patternu můžeme mít libovolné množství. Zpravidla tím vyjadřujeme, že se nějaká část na vstupu má opakovat.

```javascript
const isMatch = /^zacatek (hello)* konec$/.exec("zacatek hellohellohello konec");
```

Mimo to, lze grouping použít právě pro vycucání stringů ze vstupu. Lze použít také v IDE 
